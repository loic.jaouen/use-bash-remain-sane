# use-bash-remain-sane

How to use bash and remain sane

A personal, humble and biased collection of good practices

## why use bash ?

Because it is there, available on any unix server, it is self contained and doesn't require external libraries or specific environment, it is stable and we don't worry about a versions.  

What for and when use it? System Administration task.

## what's this page ?

I seriously dig once every six months in shell scripting and never remember the subtleties of `bash`.

It is an old and grown up tool, there are more than one way to do things and choosing is time consuming.

So I need a boiler plate to start from, and a couple of explanation to stop asking the same questions.